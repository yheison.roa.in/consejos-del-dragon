import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'dragondev.consejos.app',
  appName: 'Consejos del Dragon',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
