import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { InAppPurchase2, IAPProduct } from '@awesome-cordova-plugins/in-app-purchase-2/ngx';


import { CommonService } from 'src/app/core/services/common.service';
import { GlobalService } from 'src/app/core/services/globals.service';
import { CategoryService } from 'src/app/core/services/category.service';
import { Category } from 'src/app/shared/tips.interface';
import { NavController } from '@ionic/angular';
import { AppRoutes } from 'src/app/utils/app-routes';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // ********************
  categories: Category[] = [];
  categorySelected: Category | undefined;
  // ********************
  products: IAPProduct[] = [];
  isLoadingPurchase: boolean = false;
  private _isActivePurchase: boolean = false;
  unlockResult: BehaviorSubject<string | any> = new BehaviorSubject<string | any>(null);
  // ********************

  constructor(
    private plt: Platform,
    private navCtrl: NavController,
    private store: InAppPurchase2,
    private ref: ChangeDetectorRef,
    private categoryService: CategoryService,
    private commonService: CommonService,
    private globalService: GlobalService,
  ) {
  }

  async ngOnInit() {
    await this.getCategories();
    const platformReady = await this.plt.ready();
    if (!platformReady) return;
    this.store.verbosity = this.store.DEBUG;
    this.registerProductsStore();
    this.setupListeners();
    this.store.ready(() => {
      this.products = this.store.products;
      console.log('List Products Available',this.products);
      // this.ref.detectChanges();
      this.setPriceToProduct();
    });
  }

  async getCategories() {
    this.categories = await this.categoryService.getCategories;
  }

  async viewTipCategory() {
    const category = this.categorySelected;
    this.categoryService.setCategorySelected = category || this.categories[0];
    this.navCtrl.navigateForward(AppRoutes.tip_detail);
  }

  registerProductsStore() {
    for (const category of this.categories) {
      this.store.register({
        id: category.product_id,
        type: this.store.CONSUMABLE,
      });
    }

    this.store.refresh();
  }

  setPriceToProduct() {
    for (const index in this.categories) {
      const product = this.products.find((ele) => this.categories[index].product_id == ele.id);
      if(product) this.categories[index].price = product.price;
    }
  }

  setupListeners() {
    // CANCELED
    this.store.when('product').cancelled((product: IAPProduct) => {
      console.log('IAPProduct cancelled',product)
      this.isLoadingPurchase = false;
    });
    // ERROR
    this.store.when('product').error((product: any) => {
      console.log('IAPProduct error',product)
      const {message} = product;
      if(message){
        this.globalService.showToast(message);
      }
      this.isLoadingPurchase = false;
    });
    // FINISHED
    this.store.when('product').finished((product: IAPProduct) => {
      console.log('IAPProduct finished',product)
      this.isLoadingPurchase = false;
    });
    // VALID
    this.store.when('product').valid((product:any)=>{
      console.log('IAPProduct valid',product)
      this.isLoadingPurchase = false;
    });
    // APPROVED
    this.store.when('product').approved((product: IAPProduct) => {
      console.log('Approved===>',product)
      this.isLoadingPurchase = false;
      // Handle the product deliverable
      if (this.isActivePurchase) {
        this.isActivePurchase = false;
      }
      this.ref.detectChanges();
      this.viewTipCategory();

      return product.verify();
    }).verified((p: IAPProduct) => {
      p.finish();
    });

    // Specific query for one ID
    // this.store.when(PRODUCT_PRO_KEY).owned((p: IAPProduct) => {
    //   this.isPro = true;
    // });
  }

  async askPurchase(item: Category) {
    this.globalService.showAlert(
      'COMPRAR',
      '¿Quieres comprar el contenido?',
      [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Comprar',
          role: 'buy',
          handler: () => {
            this.categorySelected = item;
            this.purchase();
            // this.viewTipCategory();
          }
        }
      ]
    );
  }

  async purchase() {
    try {
      // const product = this.products[0];
      const product = this.products.find((ele) => this.categorySelected?.product_id == ele.id);
      console.log('Product Purchase===>',product)
      if (!product) return;
      this.store.order(product).then((p: any) => {
        console.log('Init Purchase=======>',p);
        this.isLoadingPurchase = true;
        this.isActivePurchase = true;
        // Purchase in progress!
      }, (error: any) => {
        console.log('Failed', `Failed to purchase: ${error}`);
      });
    } catch (error) {
      this.globalService.showToast('Ocurrió un error al obtener el producto de la tienda.');
      console.log('CATH ERR PURCHASE',error)
    }
  }

  // To comply with AppStore rules
  restore() {
    this.store.refresh();
  }

  public get isActivePurchase(): boolean {
    return this._isActivePurchase;
  }
  public set isActivePurchase(value: boolean) {
    this._isActivePurchase = value;
  }

}
