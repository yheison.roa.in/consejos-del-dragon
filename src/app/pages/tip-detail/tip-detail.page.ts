import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Clipboard } from '@capacitor/clipboard';
import { Share, CanShareResult } from '@capacitor/share';

import { CategoryService } from 'src/app/core/services/category.service';
import { Category, Tip } from 'src/app/shared/tips.interface';
import { AppRoutes } from 'src/app/utils/app-routes';
import { GlobalService } from 'src/app/core/services/globals.service';
import { tipsData } from 'src/app/shared/data/tips';

@Component({
  selector: 'app-tip-detail',
  templateUrl: './tip-detail.page.html',
  styleUrls: ['./tip-detail.page.scss'],
})
export class TipDetailPage implements OnInit {

  categorySelected: Category | undefined;
  tips: Tip[] = tipsData;
  tip: Tip | undefined;

  constructor(
    private navCtrl: NavController,
    private toastController: ToastController,
    private categoryService: CategoryService,
    private globalService: GlobalService,
  ) { }

  async ngOnInit() {
    this.categorySelected = await this.categoryService.getCategorySelected;
    console.log("🚀 ~ TipDetailPage ~ ngOnInit ~ this.categorySelected:", this.categorySelected)
    if(this.categorySelected){
      this.generateTip()
    }
  }

  async generateTip() {
    const tips_category = this.tips.filter((ele) => ele.category_id == this.categorySelected?.id);
    this.tip = tips_category[Math.floor(Math.random() * tips_category.length)];
    console.log("🚀 ~ TipDetailPage ~ generateTip ~ this.tip:", this.tip)
  }

  goBack() {
    this.navCtrl.navigateBack(AppRoutes.home);
  }

  async copyClipboard() {
    if(this.tip) {
      await Clipboard.write({
        string: `${this.tip.title}: ${this.tip.description}`
      });

      await this.globalService.showToast('Tip copiado al portapapeles');
    }
  }

  async shareTip() {
    if(this.tip) {
      if(!await Share.canShare()) {
        await this.globalService.showToast('No se puede compartir en este momento.');
      }
      await Share.share({
        title: this.tip.title,
        text: `${this.tip.title}: ${this.tip.description}`
      });
    }
  }

}
