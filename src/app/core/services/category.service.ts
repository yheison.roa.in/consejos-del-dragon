import { Injectable } from '@angular/core';
import { Category } from 'src/app/shared/tips.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categories: Category[] = [
    {
      id: 'vida-ordenada',
      name: 'Vida ordenada',
      subtitle: '',
      img: '/assets/images/categories/lugar-de-trabajo.png',
      product_id: 'tips_vida_ordenada_2'
    },
    {
      id: 'antes-de-dormir',
      name: 'Antes de Dormir',
      subtitle: '',
      img: '/assets/images/categories/dormido.png',
      product_id: 'tips_antes_de_dormir'
    },
    {
      id: 'mantente-productivo',
      name: 'Mantente Productivo',
      subtitle: '',
      img: '/assets/images/categories/productividad.png',
      product_id: 'tips_productivo'
    },
    {
      id: 'haz-presupuestos',
      name: 'Haz Presupuestos',
      subtitle: '',
      img: '/assets/images/categories/presupuesto.png',
      product_id: 'tips_presupuestos'
    },
  ];
  categorySelected: Category | undefined;

  constructor() { }

  get getCategories() {
    return this.categories;
  }

  get getCategorySelected() {
    return this.categorySelected;
  }

  set setCategorySelected(category: Category) {
    this.categorySelected = category;
  }
}
