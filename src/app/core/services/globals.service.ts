import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  // Data Local
  STORAGE_LOCAL_TIPS:string = 'consejos:local-data:consejos';

  constructor(
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
  ) { }

  async showToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2500,
      position: 'bottom',
      buttons: ['OK']
    });
    await toast.present();
  }

  async showAlert(header: string, message: string, buttons: Array<any>, dismissible = true){
    const alert =  await  this.alertCtrl.create({
      backdropDismiss: dismissible,
      header,
      message,
      buttons
    });
    await alert.present();
    return alert;
  }

}
